<?php

use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

app('router')->group(['prefix' => 'v1'], function (Router $r) {

    $r->post('tokens', 'Api\V1\TokenController@login');
    $r->resource('students', 'Api\V1\StudentController', ['only' => ['store']]);

});

app('router')->group(['middleware' => 'auth:api', 'prefix' => 'v1'], function (Router $r) {

    $r->post('students/{student}/password', 'Api\V1\StudentController@updatePassword');
    $r->resource('students', 'Api\V1\StudentController', ['only' => ['show', 'update']]);
    $r->resource('branches', 'Api\V1\BranchController', ['only' => ['index', 'show']]);
    $r->resource('subjects', 'Api\V1\SubjectController', ['only' => ['index']]);
    $r->get('lessons/list', 'Api\V1\LessonController@showLessons');
    $r->post('lessons/detach', 'Api\V1\LessonController@detach');
    $r->resource('lessons', 'Api\V1\LessonController', ['only' => ['index', 'store']]);
    $r->resource('cardbooks', 'Api\V1\CardbookController', ['only' => ['index']]);

});
