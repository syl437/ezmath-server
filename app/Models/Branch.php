<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function admins()
    {
        return $this->hasMany(Admin::class);
    }

    public function teachers()
    {
        return $this->belongsToMany(Teacher::class);
    }
}
