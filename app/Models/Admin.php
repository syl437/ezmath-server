<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public function branches()
    {
        return $this->belongsToMany(Branch::class);
    }
}
