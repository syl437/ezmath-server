<?php

namespace App\Http\Requests;

use Flugg\Responder\Http\MakesResponses;
use Illuminate\Foundation\Http\FormRequest;

class CreateStudent extends FormRequest
{
    use MakesResponses;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|unique:students',
            'password' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|string',
            'city' => 'required|string',
            'grade' => 'required|string',
            'level' => 'required|string',
            'school_name' => 'required|string',
            'parent_name' => 'required|string',
            'parent_phone' => 'required|string',
        ];
    }
}
