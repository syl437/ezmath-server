<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CreateStudent;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStudent $request)
    {
        $student = new Student();
        $student->fill($request->all());
        $student->save();

        return responder()->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return responder()->success($student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $student->fill($request->all());
        $student->save();

        return responder()->success()->respond();
    }

    public function updatePassword(Request $request, Student $student)
    {
        if (!Hash::check($request->get('old_password'), $student->password)){
            return responder()->error('wrong_old_password')->respond();
        }

        $student->password = bcrypt($request->get('new_password'));
        $student->save();

        return responder()->success()->respond();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
