<?php

use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $t1 = new \App\Models\Teacher();
        $t1->name = 'Teacher1';
        $t1->email = 'teacher1@test.com';
        $t1->password = bcrypt(env('DEFAULT_PASSWORD'));
        $t1->save();

        $t2 = new \App\Models\Teacher();
        $t2->name = 'Teacher2';
        $t2->email = 'teacher2@test.com';
        $t2->password = bcrypt(env('DEFAULT_PASSWORD'));
        $t2->save();

        $t3 = new \App\Models\Teacher();
        $t3->name = 'Teacher3';
        $t3->email = 'teacher3@test.com';
        $t3->password = bcrypt(env('DEFAULT_PASSWORD'));
        $t3->save();

        $b1 = \App\Models\Branch::find(1);
        $t1->branches()->attach($b1, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

        $b2 = \App\Models\Branch::find(2);
        $t2->branches()->attach($b2, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);

        $b3 = \App\Models\Branch::find(3);
        $t3->branches()->attach($b3, ['created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
    }
}
