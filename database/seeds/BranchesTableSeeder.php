<?php

use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $b1 = new \App\Models\Branch();
        $b1->title = 'סניף חיפה';
        $b1->location = 'חיפה';
        $b1->save();

        $b2 = new \App\Models\Branch();
        $b2->title = 'סניף נהריה';
        $b2->location = 'נהריה';
        $b2->save();

        $b3 = new \App\Models\Branch();
        $b3->title = 'סניף עכו';
        $b3->location = 'עכו';
        $b3->save();

        $b4 = new \App\Models\Branch();
        $b4->title = 'סניף יקנעם';
        $b4->location = 'יקנעם';
        $b4->save();
    }
}
