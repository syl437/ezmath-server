<?php

use Illuminate\Database\Seeder;

class CardbooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c1 = new \App\Models\Cardbook();
        $c1->quantity = 10;
        $c1->save();

        $c2 = new \App\Models\Cardbook();
        $c2->quantity = 12;
        $c2->save();

        $c3 = new \App\Models\Cardbook();
        $c3->quantity = 15;
        $c3->save();
    }
}
