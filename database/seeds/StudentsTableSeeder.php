<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $s1 = new \App\Models\Student();
        $s1->name = 'Student1';
        $s1->email = 'student1@test.com';
        $s1->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s1->phone = '111111111';
        $s1->city = 'חיפה';
        $s1->grade = 'א';
        $s1->level = 'א';
        $s1->school_name = 'בית ספר 1';
        $s1->parent_name = 'הורה 1';
        $s1->parent_phone = '111111111';
        $s1->save();
    
        $s2 = new \App\Models\Student();
        $s2->name = 'Student2';
        $s2->email = 'student2@test.com';
        $s2->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s2->phone = '222222222';
        $s2->city = 'חיפה';
        $s2->grade = 'ב';
        $s2->level = 'ב';
        $s2->school_name = 'בית ספר 2';
        $s2->parent_name = 'הורה 2';
        $s2->parent_phone = '222222222';
        $s2->save();
    
        $s3 = new \App\Models\Student();
        $s3->name = 'Student3';
        $s3->email = 'student3@test.com';
        $s3->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s3->phone = '333333333';
        $s3->city = 'חיפה';
        $s3->grade = 'ג';
        $s3->level = 'ג';
        $s3->school_name = 'בית ספר 3';
        $s3->parent_name = 'הורה 3';
        $s3->parent_phone = '333333333';
        $s3->save();
        
        $s4 = new \App\Models\Student();
        $s4->name = 'Student4';
        $s4->email = 'student4@test.com';
        $s4->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s4->phone = '444444444';
        $s4->city = 'נתניה';
        $s4->grade = 'ד';
        $s4->level = 'ד';
        $s4->school_name = 'בית ספר 4';
        $s4->parent_name = 'הורה 4';
        $s4->parent_phone = '444444444';
        $s4->save();
        
        $s5 = new \App\Models\Student();
        $s5->name = 'Student5';
        $s5->email = 'student5@test.com';
        $s5->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s5->phone = '555555555';
        $s5->city = 'נתניה';
        $s5->grade = 'ה';
        $s5->level = 'ה';
        $s5->school_name = 'בית ספר 5';
        $s5->parent_name = 'הורה 5';
        $s5->parent_phone = '555555555';
        $s5->save();
    
        $s6 = new \App\Models\Student();
        $s6->name = 'Student6';
        $s6->email = 'student6@test.com';
        $s6->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s6->phone = '666666666';
        $s6->city = 'נתניה';
        $s6->grade = 'ו';
        $s6->level = 'ו';
        $s6->school_name = 'בית ספר 6';
        $s6->parent_name = 'הורה 6';
        $s6->parent_phone = '666666666';
        $s6->save();
        
        $s7 = new \App\Models\Student();
        $s7->name = 'Student7';
        $s7->email = 'student7@test.com';
        $s7->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s7->phone = '777777777';
        $s7->city = 'הרצליה';
        $s7->grade = 'ז';
        $s7->level = 'ז';
        $s7->school_name = 'בית ספר 7';
        $s7->parent_name = 'הורה 7';
        $s7->parent_phone = '777777777';
        $s7->save();
    
        $s8 = new \App\Models\Student();
        $s8->name = 'Student8';
        $s8->email = 'student8@test.com';
        $s8->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s8->phone = '888888888';
        $s8->city = 'הרצליה';
        $s8->grade = 'ח';
        $s8->level = 'ח';
        $s8->school_name = 'בית ספר 8';
        $s8->parent_name = 'הורה 8';
        $s8->parent_phone = '888888888';
        $s8->save();
        
        $s9 = new \App\Models\Student();
        $s9->name = 'Student9';
        $s9->email = 'student9@test.com';
        $s9->password = bcrypt(env('DEFAULT_PASSWORD'));
        $s9->phone = '999999999';
        $s9->city = 'הרצליה';
        $s9->grade = 'ט';
        $s9->level = 'ט';
        $s9->school_name = 'בית ספר 9';
        $s9->parent_name = 'הורה 9';
        $s9->parent_phone = '999999999';
        $s9->save();
        
    }
}
